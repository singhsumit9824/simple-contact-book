from django.urls import path, include

urlpatterns = [
    path('user/', include('api.v1.account.urls')),

    # contact/
    path('', include('api.v1.contact.urls'))
]