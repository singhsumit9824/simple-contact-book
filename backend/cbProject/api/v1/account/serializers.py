from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError
from utils.utils import normalize_email

USER = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(max_length=128, style={"input_type": "password"},
                                             write_only=True, )

    class Meta:
        model = get_user_model()
        fields = ["id", "password", "confirm_password", "username", "first_name", "last_name", "mid_name", "email",
                  "is_online", ]
        read_only = ("id", "is_staff", "is_superuser", "is_online", "is_active", "")
        extra_kwargs = {
            "password": {"write_only": True},
            "confirm_password": {"write_only": True},
            "first_name": {"required": True},
            "mid_name": {"required": True},
            "last_name": {"required": True},
            "email": {"required": True},
        }

    @staticmethod
    def validate_email(email):
        email = normalize_email(email)

        # check if provide email is unique
        if USER.objects.filter(email=email).exists():
            raise ValidationError("User with email already exists.")

        return email

    # validating password
    def validate(self, attrs):
        if attrs['password'] != attrs['confirm_password']:
            raise ValidationError("Two password didn't matched")
        return attrs

    def create(self, validated_data):
        validated_data.pop("confirm_password")
        pwd = validated_data.pop("password")
        user = USER(**validated_data)

        # user.is_active = False

        user.save()
        user.set_password(pwd)
        user.save()
        return user
