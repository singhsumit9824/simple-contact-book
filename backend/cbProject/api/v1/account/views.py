from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model
from . serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status


USER = get_user_model()

class UserCreateAPIView(CreateAPIView):
    queryset = USER.objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        msg = {
            'success': 'User Created Sucessfully. Activate Account Now.',
            'data': serializer.data
        }
        return Response(msg, status=status.HTTP_201_CREATED, headers=headers)