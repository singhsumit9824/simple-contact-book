from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView,
                                            TokenVerifyView)
from . views import UserCreateAPIView

urlpatterns = [
    path("signup/", UserCreateAPIView.as_view(), name="user-signup"),
    path("signin/", TokenObtainPairView.as_view(), name="user-signin"),
    path("refresh-token/", TokenRefreshView.as_view(), name="token_refresh"),
    path("verify-token/", TokenVerifyView.as_view(), name="token_verify"),
]
