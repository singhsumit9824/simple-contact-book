from rest_framework.permissions import BasePermission


class IsCreator(BasePermission):

    def has_permission(self, request, view):
        print("has first name ", hasattr(request.user, 'first_name'))
        return request.user.is_authenticated and hasattr(request.user, 'contact_set')

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated and hasattr(request.user, 'contact_set') and (
                    obj.creator_id == request.user.id)
