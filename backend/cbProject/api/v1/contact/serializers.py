from contact.models import Contact
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.contrib.auth import get_user_model
from utils.utils import normalize_email

USER = get_user_model()


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ["id", "first_name", "mid_name", "last_name", "phone", "email", "creator_id"]
        read_only = ("id","creator_id")
        extra_kwargs = {
            "mid_name": {
                "required": True,
            }
        }

    # # Overriding get fileds for diff methods
    # def get_fields(self):
    #     fields = super().get_fields()
    #
    #     # get request object from context
    #     request = self.context.get('request')
    #     if request and request.method.lower() == "post":
    #         # serializer email fields with EmailSerializer
    #         # fields["email"] = EmailAccountSerializer(request.data)
    #         fields["emails"] = serializers.ListField()
    #         fields["phones"] = serializers.ListField()
    #     return fields

    @staticmethod
    def validate_email(email):
        email = normalize_email(email)

        # check if provide email is unique
        if Contact.objects.filter(email=email).exists():
            raise ValidationError("Contact with email already exists.")

        return email

    def validate(self, attrs):
        # get request object from context
        request = self.context.get('request')
        # add creator id
        attrs.update(creator_id=request.user.id)
        return super().validate(attrs)
