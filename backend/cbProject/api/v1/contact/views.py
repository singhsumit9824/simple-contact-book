from rest_framework import viewsets
from .serializers import ContactSerializer
from contact.models import Contact
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .permissions import IsCreator


class ContactModelViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = [IsAuthenticated, ]
    lookup_field = 'id'
    lookup_url_kwarg = "id"

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request
        return context

    def get_queryset(self):
        filter_dict = dict()
        user = self.request.user
        if hasattr(user, "contact_set"):
            filter_dict.update(creator_id=user.id)
        return Contact.objects.filter(**filter_dict)

    # authenticated user can view, only creator can do update,create, and delete
    def get_permissions(self):
        if self.action == 'destroy' or self.action == 'update' or self.action == 'partial_update' or self.action == 'list' or self.action == 'retrieve':
            self.permission_classes += [IsCreator, ]
        else:
            # able to read for other authenticated
            self.permission_classes += [IsAuthenticated, ]

        return [permission() for permission in self.permission_classes]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
