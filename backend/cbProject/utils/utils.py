from rest_framework.exceptions import ValidationError


def normalize_email(email):
    try:
        email_name, domain_part = email.strip().rsplit('@', 1)
    except ValueError as e:
        raise ValidationError(f"email field : {email} must have '@'")
    else:
        if domain_part.lower() not in ['gmail.com', 'outlook.com']:
            raise ValidationError("Your email must be from either gmail or outlook provider.")

        email = email_name + '@' + domain_part.lower()
    return email