from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    mid_name = models.CharField(blank=True, null=True, max_length=50)
    is_online = models.BooleanField(default=False, blank=True)


