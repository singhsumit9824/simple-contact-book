from django.db import models
from django.contrib.auth import get_user_model

USER = get_user_model()

# Create your models here.
class Contact(models.Model):
    first_name = models.CharField(max_length=20)
    mid_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=20)

    phone = models.CharField(max_length=10)
    email = models.EmailField()
    creator = models.ForeignKey(USER, on_delete=models.CASCADE)

